//This is the main class for our game 

import ddf.minim.*;
 
Minim minim;
AudioPlayer song;


void setup(){
  size(600,800);
  minim = new Minim(this);
  song = minim.loadFile("background.wav");
  song.play();
  song.loop();
}
void draw() {
  //blank the background 
  background(0);
}
